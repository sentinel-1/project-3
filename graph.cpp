#include <iostream>
#include <fstream>
#include <limits>
#include <iomanip>
#include "graph.h"
#include "heap.h"
using namespace std;


VERTEX* V = NULL;
int countPath = 0;

int arrr[100];
int computed[100];
int compCount = 0;
int dijkstra(int n, ptr_node* ptrNode, int s, int t, int flag) {

	ptr_node node = NULL;

	V = (VERTEX*)calloc(n + 1, sizeof(VERTEX));

	for (int i = 1; i <= n; i++) {
		V[i].color = white;
		V[i].dist = std::numeric_limits<float>::max();
		V[i].pi = 0;
		V[i].position = 0;
	}
	V[s].dist = 0;
	V[s].color = gray;
	HEAP* heap = (HEAP*)calloc(1, sizeof(HEAP));
	heap->capacity = n;
	heap->size = 0;
	heap->H = (ptr_element*)calloc(n + 1, sizeof(ptr_element));
	heap->H[0] = (ptr_element)malloc(sizeof(ELEMENT));
	heap->H[0]->key = 1000;
	heap->H[0]->vertex = 1000;
	insert(heap, V[s].dist, s, V);
	if (flag == 1) {

		printf("Insert vertex %d, key=%12.4f\n", s, V[s].dist);


		computed[compCount] = s;
		compCount++;
	}
	while (heap->size != 0) {
		int edge_source = extractMin(heap, V);
		if (flag == 1) {
			
			printf("Delete vertex %d, key=%12.4f\n", edge_source, V[edge_source].dist);
		}
		V[edge_source].color = black;
		if (edge_source == t) {
			free(heap);
			return 0;
		}
		node = ptrNode[edge_source];
		while (node) {
			if (V[node->edge_destination].color == white) {

				V[node->edge_destination].dist = V[edge_source].dist + node->edge_weight;
				V[node->edge_destination].pi = edge_source;
				V[node->edge_destination].color = gray;
				insert(heap, V[node->edge_destination].dist, node->edge_destination, V);
				if (flag == 1) {
					printf("Insert vertex %d, key=%12.4f\n", node->edge_destination, V[node->edge_destination].dist);
					computed[compCount] = node->edge_destination;
					compCount++;
				}
			}
			
			else if (V[node->edge_destination].dist > (V[edge_source].dist + node->edge_weight)) {
				float oldDist = V[node->edge_destination].dist;
				V[node->edge_destination].dist = V[edge_source].dist + node->edge_weight;
				V[node->edge_destination].pi = edge_source;
				decreaseKey(heap, V[node->edge_destination].position, V[node->edge_destination].dist, V);
				if (flag == 1) {
					printf("Decrease key of vertex %d, from %12.4f to %12.4f\n", node->edge_destination, oldDist, V[node->edge_destination].dist);
					
				}
			}
			
			node = node->next;
			
		}

	}
	
	free(heap);
	return 1;
}
void freeVertex() {
	freeV(V);
}
int isComputed(int vertex) {
	for (int i = 0; i < compCount; i++) {
		if (computed[i] == vertex) {
			return 1;
		}
	}
	return 0;
	compCount = 0;
}
void readGraph(string file) {
	ifstream inFile;
	ptr_node node;
	ptr_node* ptrNode;
	int n, m, id, edge_source, edge_destination, edge_weight;
	inFile.open(file);
	if (inFile.is_open()) {
		inFile >> n >> m;

		ptrNode = (ptr_node*)calloc(n + 1, sizeof(ptr_node));
		for (int i = 0; i < m; i++) {
			inFile >> id >> edge_source >> edge_destination >> edge_weight;
			node = (ptr_node)malloc(sizeof(NODE));
			node->edge_destination = edge_destination;
			node->edge_weight = edge_weight;
			node->next = ptrNode[edge_source];
			ptrNode[edge_source] = node;

		}
		
		inFile.close();
	}

}
float getWeight(int vertex) {
	return V[vertex].dist;
}
int getColor(int vertex) {
	return V[vertex].color;
}
void getPath(int d, int s) {
	int pi;
	pi = V[d].pi;
	if (d != s) {

		countPath++;

		arrr[countPath] = d;

		getPath(pi, s);
	}
	else {
		
		for (int i = countPath; i > 0; i--) {
			if (i == 1) {
				cout << arrr[i] << ">";
			}
			else {
				cout << arrr[i] << ", ";
			}

		}
		countPath = 0;
	}



}
