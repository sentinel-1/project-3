#include <iostream>
#include <string>
#include "util.h"
#include "heap.h"
#include "graph.h"
using namespace std;
string filename = "";
int main(int argc, char* argv[]) {
	if (argc > 1) {
		filename = argv[1];
	}
	string graphType = argv[2];
	char c;
	int source, s, d, dest, flag, notFound = 0;
	float weight;
	commands command;
	int exists;
	ifstream inFile;
	ptr_node node = NULL;
	ptr_node* ptrNode = NULL;
	int n, m, id, edge_source, edge_destination;
	float edge_weight;
	inFile.open(filename);
	if(!inFile.good()){

		cout<<"File opening error"<<endl;
		return -1;
	}
	if (inFile.is_open()) {
		inFile >> n >> m;
		ptrNode = (ptr_node*)calloc(n + 1, sizeof(ptr_node));
		for (int i = 0; i < m; i++) {
			inFile >> id >> edge_source >> edge_destination >> edge_weight;

			node = (ptr_node)malloc(sizeof(NODE));

			node->edge_destination = edge_destination;
			node->edge_weight = edge_weight;
			node->next = ptrNode[edge_source];

			ptrNode[edge_source] = node;

			if (graphType.compare("undirected") == 0) {
				node = (ptr_node)malloc(sizeof(NODE));
				node->edge_destination = edge_source;
				node->edge_weight = edge_weight;
				node->next = ptrNode[edge_destination];
				ptrNode[edge_destination] = node;
			}
		}
		inFile.close();
	}
	while (1) {
		c = inputCommand(&source, &s, &d, &dest, &flag);
		command = current(c);
		switch (command) {
		case STOP:
			cout << "Query: stop" << endl;
			for (int i = 0; i < n; i++) {
				free(ptrNode[i]);
			}
			free(node);
			freeVertex();
			exit(0);
			break;
		case WRITE:
			cout << "Query: write path " << s << " " << d << endl;
			if (notFound == 1) {
				cout << "Error: no path computation done" << endl;
				break;
			}
			if (s != source || d > n) {
				cout << "Error: invalid source destination pair" << endl;
				break;
			}

			weight = getWeight(d);
			if (isComputed(d) == 0 && exists == 0) {
				cout << "No " << s << "-" << d << " path has been computed." << endl;
				break;
			}
			if (weight > 100000) {
				cout << "No " << s << "-" << d << " path exists." << endl;
				break;
			}

			if (getColor(d) == gray) {
				cout << "Path not known to be shortest: <" << s << ", ";
			}
			if (getColor(d) == black) {
				cout << "Shortest path: <" << s << ", ";
			}
			getPath(d, s);
			cout << endl;
			printf("The path weight is: %12.4f\n", weight);
			break;
			case FIND:
				notFound = 0;
				cout << "Query: find " << source << " " << dest << " " << flag << endl;
				if (source > n || dest == source || flag > 1 || flag < 0) {
					cout << "Error: invalid find query" << endl;
					notFound = 1;
					break;
				}
				freeVertex();
				exists = dijkstra(n, ptrNode, source, dest, flag);
				break;
		case NONE:
			break;
		}
	}



	return 0;
}
