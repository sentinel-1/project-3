#include <iostream>
#include <limits>
#include "heap.h"
using namespace std;
ptr_element element;
int getParent(int i) {
	return (i / 2);
}
int right(int i) {
	return ((i*2) + 1);
}
int left(int i) {
	return (i*2);
}
int decreaseKey(HEAP* heap, int position, float newKey, ptrVertex V) {
	ptr_element temp;
	if (position < 1 || position > heap->size || newKey > heap->H[position]->key) {
		return 1;
	}
	heap->H[position]->key = newKey;
	while ((position > 1) && (heap->H[getParent(position)]->key >= heap->H[position]->key)) {
		temp = heap->H[position];
		heap->H[position] = heap->H[getParent(position)];
		heap->H[getParent(position)] = temp;
		V[heap->H[position]->vertex].position = getParent(position);
		V[heap->H[getParent(position)]->vertex].position = position;
		position = getParent(position);
	}
	return 0;
}


int extractMin(HEAP* heap, ptrVertex V) {
	int min;
	if (heap->size < 1) {
		cout << "Error: heap empty" << endl;
		return 0;
	}
	else {
		min = heap->H[1]->vertex;
		heap->H[1]->vertex = heap->H[heap->size]->vertex;
		heap->H[1]->key = heap->H[heap->size]->key;
		V[heap->H[1]->vertex].position = V[heap->H[heap->size]->vertex].position;
		heap->size--;
		minHeapify(heap, 1, V);
		return min;
	}
}
void insert(HEAP* heap, float key, int value, ptrVertex V) {
	element = (ptr_element)malloc(sizeof(ELEMENT));
	element->vertex = value;
	element->key = key;
	heap->size++;
	heap->H[heap->size] = element;
	int i = heap->size - 1;
	decreaseKey(heap, i + 1, key, V);
}
void minHeapify(HEAP* heap, int i, ptrVertex V) {
	int l = left(i);
	int r = right(i);
	int smallest, temp;
	if ((l <= heap->size) && (heap->H[l]->key < heap->H[i]->key)) {
		smallest = l;
	}
	else {
		smallest = i;
	}
	if ((r <= heap->size) && (heap->H[r]->key < heap->H[smallest]->key)) {
		smallest = r;
	}
	if (smallest != i) {
		temp = heap->H[i]->key;
		heap->H[i]->key = heap->H[smallest]->key;
		heap->H[smallest]->key = temp;
		V[heap->H[i]->vertex].position = smallest;
		V[heap->H[smallest]->vertex].position = i;
		minHeapify(heap, smallest, V);
	}
}
void printHeap(HEAP* H) {
	cout << "capacity=" << H->capacity << ", size=" << H->size << endl;
	for (int i = 1; i <= H->size; i++) {
		if (i == (H->size)) {
			cout << "key[" << H->H[i]->vertex << "]= " << H->H[i]->key << endl;
		}
		else {
			cout << "key[" << H->H[i]->vertex << "]= " << H->H[i]->key << ", " << endl;
		}
	}
}
int getPi(int vertex, VERTEX* V) {
	return V[vertex].pi;
}
VERTEX* getGraph(VERTEX* V) {
	return V;
}
void freeV(VERTEX* V) {
	free(V);
}
void freeE() {
	free(element);
}
