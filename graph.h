#include <iostream>
#include <fstream>
#include <string>

using namespace std;
typedef struct Vert {
	int color;
	int pi;
	float dist;
}VERT;
typedef VERT* ptrVert;
typedef struct Node {
	int edge_destination;
	float edge_weight;
	Node* next;
}NODE;
typedef NODE* ptr_node;

int dijkstra(int n, ptr_node* ptrNode, int s, int t, int flag);
void readGraph(string file);
float getWeight(int vertex);
void getPath(int d, int s);
int getColor(int vertex);
int isComputed(int vertex);
void freeVertex();
