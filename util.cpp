#include <iostream>
#include <string>
#include "util.h"
using namespace std;


int inputCommand(int* source, int* s, int* d, int* dest, int* flag) {
	string input;
	string str_in;
	string arrr[10] = {};
	int i = 0;
	char value = ' ';
	while (1) {
		cin >> input;
		while (input.compare("\n") == 0 || input.compare(" ") == 0) {
			cin >> input;

		}
		if (input.compare("find") == 0) {
			getline(cin, str_in, '\n');
			string delimeter = " ";
			str_in += delimeter;
			size_t position = 0;
			std::string::size_type sz;
			string token;
			while ((position = str_in.find(delimeter)) != std::string::npos) {
				token = str_in.substr(0, position);
				arrr[i] = token;
				str_in.erase(0, position + delimeter.length());
				i++;
			}

			*source = stoi(arrr[1], &sz);
			*dest = stoi(arrr[2], &sz);
			if (arrr[3].compare("") == 0) {
				break;
			}
			*flag = stoi(arrr[3], &sz);
			value = 'f';
			break;

		}
		if (input.compare("path") == 0) {
			cin >> *s;
			cin >> *d;
			value = 'p';
			break;
		}
		if (input.compare("stop") == 0) {
			value = 's';
			break;
		}
	}
	return value;

}
commands current(char c) {
	commands comm = NONE;
	if (c == 'f') {
		comm = FIND;
	}
	if (c == 'p') {
		comm = WRITE;
	}
	if (c == 's') {
		comm = STOP;
	}
	return comm;

}
